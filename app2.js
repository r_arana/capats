function obtenerinformacio(nombre, apellido, segundoApellido) {
    if (apellido === void 0) { apellido = "Arana"; }
    if (segundoApellido) {
        return nombre + " " + apellido + " " + segundoApellido;
    }
    else {
        return nombre + " " + apellido;
    }
}
console.log(obtenerinformacio("Ricardo", "Arana", "Guerrero"));
console.log(obtenerinformacio("Ricardo", "Arana Reyes"));
console.log(obtenerinformacio("Ricardo"));
