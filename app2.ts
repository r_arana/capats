function obtenerinformacio( nombre?: string, apellido: string = "Arana", segundoApellido?: string ){

 if(segundoApellido) {
     return `${nombre} ${apellido} ${segundoApellido}`
 } else {
    return `${nombre} ${apellido}`;
 }

}

console.log(obtenerinformacio("Ricardo", "Arana", "Guerrero"));
console.log(obtenerinformacio("Ricardo", "Arana Reyes"));
console.log(obtenerinformacio("Ricardo"));